/* hello.c */

#include <stdio.h>

int main()
{
	printf("Hello World!\n");

	double a = 0.5, b = 10.5;
	int c = 215, d;
	char e = 'A';

	printf("-------------\n");

	printf("%-5.2f + %10.2f = %5.2f\n", a, b, a + b);
	d = c + 11;
	printf("cの値は%dでこれに11を加えると%dになります\n", c, d);
	printf("eには\"%c\"が代入されています\n", e);

	printf("-------------\n");

	int aa = 10, bb = 3;
	double cc;

	cc = aa / bb;

	printf("int同士の割り算\t"
		"%d ÷ %d =%f\n",aa,bb,cc);

	int g = 10;
	double h = 2.35;

	printf("intとdoubleの足し算\t"
		"%d + %f = %f\n", g, h, g + h);

	printf("インクリメント\t"
		"g++: g= %d -> %d\n", g++,g);
	printf("インクリメント\t"
		"++aa: aa= %d -> %d\n", ++aa, aa);

	printf("----size of---------\n");
	char i = 'A';
	short j = 50;
	int k = -100;
	unsigned int l = 128;
	float m = 0.5f;
	double n = 2568.2;
	long double o = 1.258E-25;
	size_t p;

	p = sizeof i;
	printf("size of char =%d bytes\n", p);
	p = sizeof j;
	printf("size of short =%d bytes\n", p);
	p = sizeof k;
	printf("size of int =%d bytes\n", p);
	p = sizeof l;
	printf("size of unsigned int =%d bytes\n", p);
	p = sizeof m;
	printf("size of float =%d bytes\n", p);
	p = sizeof n;
	printf("size of double =%d bytes\n", p);
	p = sizeof o;
	printf("size of long double =%d bytes\n", p);
	p = sizeof(size_t);
	printf("size of size_t =%d bytes\n", p);







	return 0;
}